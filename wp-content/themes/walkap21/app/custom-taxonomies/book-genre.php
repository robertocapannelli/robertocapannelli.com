<?php
// Register Custom Taxonomy
function book_genre_taxonomy() {

    $labels  = array( 'name'                       => _x( 'Genere libro', 'Taxonomy General Name', 'sage' ),
                      'singular_name'              => _x( 'Genere libro', 'Taxonomy Singular Name', 'sage' ),
                      'menu_name'                  => __( 'Genere', 'sage' ),
                      'all_items'                  => __( 'Tutte le Generi', 'sage' ),
                      'parent_item'                => __( 'Genere genitore', 'sage' ),
                      'parent_item_colon'          => __( 'Genere genitore:', 'sage' ),
                      'new_item_name'              => __( 'Nuovo nome Genere', 'sage' ),
                      'add_new_item'               => __( 'Aggiungi nuovo Genere', 'sage' ),
                      'edit_item'                  => __( 'Modifica Genere', 'sage' ),
                      'update_item'                => __( 'Aggiungi Genere', 'sage' ),
                      'view_item'                  => __( 'Vedi Genere', 'sage' ),
                      'separate_items_with_commas' => __( 'Generi separati da una virgola', 'sage' ),
                      'add_or_remove_items'        => __( 'Aggiungi o rimuovi Generi', 'sage' ),
                      'choose_from_most_used'      => __( 'Scegli tra le più utilizzate', 'sage' ),
                      'popular_items'              => __( 'Generi popolari', 'sage' ),
                      'search_items'               => __( 'Cerca Generi', 'sage' ),
                      'not_found'                  => __( 'Non trovata', 'sage' ),
                      'no_terms'                   => __( 'Nessun Genere', 'sage' ),
                      'items_list'                 => __( 'Lista Generi', 'sage' ),
                      'items_list_navigation'      => __( 'Lista navigazione Generi', 'sage' ), );
    $rewrite = array( 'slug'         => 'libri/genere',
                      'with_front'   => true,
                      'hierarchical' => true, );
    $args    = array( 'labels'            => $labels,
                      'hierarchical'      => true,
                      'public'            => true,
                      'show_ui'           => true,
                      'show_admin_column' => true,
                      'show_in_nav_menus' => true,
                      'show_tagcloud'     => true,
                      'rewrite'           => $rewrite,
                      'show_in_rest'      => true, );
    register_taxonomy( 'book-genre', array( 'book' ), $args );

}

add_action( 'init', 'book_genre_taxonomy', 0 );
