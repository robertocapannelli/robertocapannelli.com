<?php
// Register Custom Taxonomy
function project_tag_taxonomy() {

    $labels = array(
        'name'                       => _x( 'Tag Progetti', 'Taxonomy General Name', 'sage' ),
        'singular_name'              => _x( 'Tag Progetto', 'Taxonomy Singular Name', 'sage' ),
        'menu_name'                  => __( 'Tag', 'sage' ),
        'all_items'                  => __( 'Tutti i tag', 'sage' ),
        'parent_item'                => __( 'Tag genitore', 'sage' ),
        'parent_item_colon'          => __( 'Tag genitore:', 'sage' ),
        'new_item_name'              => __( 'Aggiungi nome del tag', 'sage' ),
        'add_new_item'               => __( 'Aggiungi nuovo tag', 'sage' ),
        'edit_item'                  => __( 'Modifica tag', 'sage' ),
        'update_item'                => __( 'Aggiorna tag', 'sage' ),
        'view_item'                  => __( 'Vedi tag', 'sage' ),
        'separate_items_with_commas' => __( 'Separa i tag con una virgola', 'sage' ),
        'add_or_remove_items'        => __( 'Aggiungi o rimuovi tag', 'sage' ),
        'choose_from_most_used'      => __( 'Scegli tra i più utilzizati', 'sage' ),
        'popular_items'              => __( 'Tag popolari', 'sage' ),
        'search_items'               => __( 'Cerca tag', 'sage' ),
        'not_found'                  => __( 'Non trovato', 'sage' ),
        'no_terms'                   => __( 'Nessun tag', 'sage' ),
        'items_list'                 => __( 'Lista tag', 'sage' ),
        'items_list_navigation'      => __( 'Navigazione lista tag', 'sage' ),
    );
    $rewrite = array(
        'slug'                       => 'progetti/tag',
        'with_front'                 => true,
        'hierarchical'               => true,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
        'rewrite'                    => $rewrite,
        'show_in_rest'        => true,
    );
    register_taxonomy( 'project_tag', array( 'project' ), $args );

}
add_action( 'init', 'project_tag_taxonomy', 0 );
