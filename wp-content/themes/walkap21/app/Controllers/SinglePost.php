<?php


namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;


class SinglePost extends Controller {

    public function getRelatedPosts() {
        $tags = wp_get_post_terms( get_the_ID(), 'post_tag', [ 'fields' => 'ids' ] );

        $args = [
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => 4,
            'post__not_in'   => [ get_the_ID() ],
            'tax_query'      => [
                [
                    'taxonomy' => 'post_tag',
                    'field'    => 'id',
                    'terms'    => $tags
                ],
            ]
        ];

        return new WP_Query( $args );
    }

    public function getReferralLinks() {
        if ( !class_exists( 'ACF' ) ) {
            return;
        }

        return get_field( 'referrals' );
    }
}
