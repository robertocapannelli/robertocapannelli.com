<?php


namespace App\Controllers\Partials;


trait Books {
    public function getLatestBooks() {
        $transient = 'latest_books';

        if ( WP_DEBUG || false === ( $books = get_transient( $transient ) ) ) {
            $args = [
                'post_type'      => 'book',
                'post_status'    => 'publish',
                'posts_per_page' => 6,
                'order'          => 'DESC',
                'orderby'        => 'date',
                'hide_empty'     => true,
            ];

            $books = new \WP_Query( $args );
            set_transient( $transient, $books, MONTH_IN_SECONDS );
        }

        return $books;
    }

    public function getBookTerms() {
        $args = [ 'taxonomy'   => 'book-genre',
                  'hide_empty' => true,
                  'order_by'   => 'name',
                  'parent'     => 0 ];

        $terms = get_terms( $args );

        if ( empty( $terms ) || is_wp_error( $terms ) ) {
            return;
        }

        return $terms;
    }
}
