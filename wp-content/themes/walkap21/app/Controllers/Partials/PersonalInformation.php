<?php

namespace App\Controllers\Partials;

trait PersonalInformation {

    public function getVatNumber() {
        if(!class_exists('ACF')){
            return;
        }

        return get_field( 'vat_number', 'option' );
    }

    public function getName(){
        if(!class_exists('ACF')){
            return;
        }

        return get_field('name', 'option');
    }
}
