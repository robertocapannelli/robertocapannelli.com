<?php


namespace App\Controllers;

use App\Controllers\Partials\Posts;
use Sober\Controller\Controller;

class ArchivePost extends Controller {
    use Posts;
}
