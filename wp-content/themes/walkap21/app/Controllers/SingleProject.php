<?php

namespace app\Controllers;

use Sober\Controller\Controller;

class SingleProject extends Controller
{
    public function getProjectUrl() {
        if (!class_exists('ACF')) return;

        return get_field('field_674b45378a840');
    }

    public function getProjectDuration() {
        if (!class_exists('ACF')) return;
        if (!$duration_group = get_field('field_674b45518a841')) return;
        $duration = '';

        if ($duration_group['starting_date']) {
            $duration .= $duration_group['starting_date'];
        } else {
            return null;
        }

        if ($duration_group['ending_date']) {
            $duration .= ' - ' . $duration_group['ending_date'];
        } else {
            $duration .= ' - Presente';
        }

        return $duration;
    }

    public function getProjectContributors() {
        if (!class_exists('ACF')) return;

        return get_field('field_674b46398a844');
    }

    public function getProjectCollaboration() {
        if (!class_exists('ACF')) return;

        return get_field('field_674b48488a847');
    }

    public function getProjectKind() {
        if (!class_exists('ACF')) return;

        return get_field('field_67713da1ddd0e');
    }

    public function getProjectLogo() {
        if (!class_exists('ACF')) return;

        return get_field('field_677155064e3b5');
    }
}
