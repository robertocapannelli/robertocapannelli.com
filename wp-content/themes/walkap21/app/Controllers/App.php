<?php

namespace App\Controllers;

use App\Controllers\Partials\PersonalInformation;
use App\Controllers\Partials\Posts;
use App\Controllers\Partials\Social;
use Sober\Controller\Controller;

class App extends Controller {
    use PersonalInformation;
    use Social;
    use Posts;

    public function siteName() {
        return get_bloginfo( 'name' );
    }

    public static function title() {
        if ( is_home() ) {
            if ( $home = get_option( 'page_for_posts', true ) ) {
                return get_the_title( $home );
            }

            return __( 'Ultimi articoli', 'sage' );
        }
        if ( is_archive() ) {
            return get_the_archive_title();
        }
        if ( is_search() ) {
            return sprintf( __( 'Risultati di ricerca per: %s', 'sage' ), get_search_query() );
        }
        if ( is_404() ) {
            return __( 'Non trovato', 'sage' );
        }

        return get_the_title();
    }

    /**
     * Primary Nav Menu arguments
     *
     * @return array
     */
    public function primarymenu() {
        $args = array(
            'theme_location' => 'primary_navigation',
            'menu_class'     => 'navbar-nav',
        );

        return $args;
    }
}
