<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continua', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

add_filter('sage/display_sidebar', function ($display) {
    static $display;

    isset($display) || $display = in_array(true, [
        // The sidebar will be displayed if any of the following return true
        is_singular('post'),
        is_home(),
        is_404(),
        is_category(),
        is_tag(),
        is_page(),
        is_page_template('views/template-custom.blade.php')
    ]);

    return $display;
});

function my_theme_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
}

add_filter( 'get_the_archive_title', __NAMESPACE__.'\\my_theme_archive_title' );

/**
 * Save ACF settings in JSON file
 */
add_filter( 'acf/settings/save_json', function( $path ) {
    $path = dirname( __FILE__ ) . '/acf/json';

    return $path;
} );

/**
 * Load ACF setting from JSON file
 */
add_filter( 'acf/settings/load_json', function( $paths ) {
    unset( $paths[ 0 ] );
    $paths[] = dirname( __FILE__ ) . '/acf/json';

    return $paths;
} );

/**
 * Inject the table of content on each post.
 */

function get_entry_content_titles( $content ) {
    if ( is_single() && get_post_type() === 'post' ) {
        $table_of_contents = '<section class="widget-table-of-contents py-4"><h3>Indice</h3><ul class="list-unstyled">';
        $content           = preg_replace_callback( '#<(h[1-6])(.*?)>(.*?)</\1>#si', function ( $matches ) use ( &$table_of_contents ) {
            $tag               = $matches[ 1 ];
            $title             = strip_tags( $matches[ 3 ] );
            $has_id            = preg_match( '/id=(["\'])(.*?)\1[\s>]/si', $matches[ 2 ], $matched_ids );
            $id                = $has_id ? $matched_ids[ 2 ] : sanitize_title( $title );
            $table_of_contents .= sprintf( '<li class="item-%s">- <a href="#%s">%s</a></li>',$tag, $id, $title );

            if ( $has_id ) {
                return $matches[ 0 ];
            }

            return sprintf( '<%s%s id="%s">%s</%s>', $tag, $matches[ 2 ], $id, $matches[ 3 ], $tag );

        }, $content, -1, $count );

        $table_of_contents .= '</ul></section>';

        if ( $count < 2 ) {
            return $content;
        }

        return $table_of_contents . $content;
    }

    return $content;
}

add_filter( 'the_content', __NAMESPACE__ . '\\get_entry_content_titles' );
