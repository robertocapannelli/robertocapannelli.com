<div class="bg-dark rounded p-4 mb-4">
  <h4 class="text-white mb-4 h1" style="font-weight: 700;">
    Vuoi realizzare un <i>progetto</i> come {{is_single() ? 'questo' : 'questi'}}?
  </h4>
  <div class="d-flex flex-column flex-sm-row justify-sm-content-start">
    <a href="/scrivimi" class="btn btn-light d-inline-block mb-3 mb-sm-0 mr-0 mr-sm-3" style="font-weight: bold;">Contattami <i class="bi bi-arrow-up-right"></i></a>
    <a href="https://supernova.blue" class="btn btn-referral d-inline-block" style="font-weight: bold;" target="_blank">Visita il sito della mia agenzia <i class="bi bi-arrow-up-right"></i></a>
  </div>
</div>
