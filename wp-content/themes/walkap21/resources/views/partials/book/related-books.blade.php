@if($get_related_books->have_posts())
  @if($get_related_books->post_count > 1)
    <section class="related-books mb-4 py-5">
      <h3 class="mb-3">Scopri altri libri che sto leggendo</h3>
      <div class="swiper-container related-slider" id="related-slider">
        <div class="swiper-wrapper">
          @while($get_related_books->have_posts())
            @php $get_related_books->the_post() @endphp
            <div class="swiper-slide">
              <article @php post_class('mb-4 border-primary border p-3 h-100 rounded') @endphp itemscope="" itemtype="https://schema.org/Book">
                @if(has_post_thumbnail())
                  @php $alt_attribute = get_the_title() . ' - ' . (\App\Controllers\FrontPage::getAuthor() ?? '')@endphp
                  <figure class="mb">
                    <a href="{{get_permalink()}}">
                      {!! get_the_post_thumbnail(null, 'book_thumbnail', ['class' => 'img-fluid w-100', 'itemprop' => 'image', 'alt' => $alt_attribute]) !!}
                    </a>
                  </figure>
                @endif
                <header>
                  <h4 class="mb-1" itemprop="name">
                    <a href="{{ get_permalink() }}">
                      {!! get_the_title() !!}
                    </a>
                  </h4>
                  <h4 class="h6 text-muted" itemprop="author" itemscope itemtype="https://schema.org/Person">
                    <small>{{\App\Controllers\FrontPage::getAuthor()}}</small>
                  </h4>
                </header>
              </article>
            </div>
          @endwhile
        </div>
      </div>
    </section>
    @php wp_reset_query() @endphp
  @endif
@endif
