@if($get_latest_books->have_posts())
  <section class="latest-books mb-4">
    <h2>Libri che leggo</h2>
    <div class="swiper-container books-slider mb-4" id="books-slider">
      <div class="swiper-wrapper">
        @while($get_latest_books->have_posts())
          @php $get_latest_books->the_post(); @endphp
          <div class="swiper-slide">
            <article itemscope="" itemtype="https://schema.org/Book">
              @if(has_post_thumbnail())
                @php $alt_attribute = get_the_title() . ' - ' .  (\App\Controllers\FrontPage::getAuthor() ?? '')@endphp
                <figure class="mb-0">
                  <a href="{{get_permalink()}}" class="h-100 d-block">
                    {!! get_the_post_thumbnail(null, 'book_thumbnail', ['class' => 'img-fluid w-100 rounded', 'itemprop' => 'image', 'alt' => $alt_attribute]) !!}
                  </a>
                </figure>
              @endif
              <div class="border-primary border p-3 sr-only">
                <header>
                  <h2 class="entry-title" itemprop="name">
                    <a href="{{ get_permalink() }}">
                      {!! get_the_title() !!}
                    </a>
                  </h2>
                  <h4 class="h6 text-muted" itemprop="author" itemscope itemtype="https://schema.org/Person">
                    <small>{{\App\Controllers\FrontPage::getAuthor()}}</small>
                  </h4>
                </header>
                <div class="entry-summary">
                  @php the_excerpt() @endphp
                </div>
                <footer>
                  <small class="d-block text-muted text-right">
                    {{\App\Controllers\FrontPage::getPublicationYear()}}
                  </small>
                </footer>
              </div>
            </article>
          </div>
        @endwhile
      </div>
    </div>
    @php wp_reset_postdata() @endphp
    <a href="{{get_post_type_archive_link('book')}}" class="btn btn-primary">
      Vedi tutti i libri <i class="bi bi-arrow-up-right"></i>
    </a>
  </section>
@endif
