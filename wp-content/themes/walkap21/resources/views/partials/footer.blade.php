<footer class="content-info">
  <div id="bottom-footer" class="border-top border-primary py-3">
    <div class="container d-flex flex-column align-items-center flex-lg-row align-items-lg-center justify-content-lg-between">

      <div class="mb-1 mb-lg-0">
        <small>
          Made in Italy with
        </small>
        <i class="bi bi-suit-heart-fill text-danger"></i>
      </div>


      @if($get_vat_number)
        <small>P.I.V.A.{{$get_vat_number}}</small>
      @endif

      @if($get_social_links)
        <ul class="list-unstyled list-inline mb-0 mt-1 mt-lg-0">
          @foreach($get_social_links as $link)
            <li class="list-inline-item">
              <a href="{{$link['url']}}" target="_blank" rel="external">
                <i class="bi bi-{{strtolower($link['name'])}}"></i>
              </a>
            </li>
          @endforeach
        </ul>
      @endif
    </div>
  </div>
</footer>
