@include('partials.post.latest-posts-sidebar')
@if(is_single())
  @include('partials.post.sidebar-referrals')
@endif
@php
  dynamic_sidebar('sidebar-primary')
@endphp
