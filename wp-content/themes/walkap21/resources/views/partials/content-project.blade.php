@php
  $get_project_url = \App\Controllers\ArchiveProject::getProjectURL(get_the_ID());
  $get_project_duration = \App\Controllers\ArchiveProject::getProjectDuration(get_the_ID());
  $get_project_contributors = \App\Controllers\ArchiveProject::getProjectContributors(get_the_ID());
  $get_project_collaboration = \App\Controllers\ArchiveProject::getProjectCollaborations(get_the_ID());
  $get_project_kind = \App\Controllers\ArchiveProject::getProjectKind(get_the_ID());
  $get_project_logo = \App\Controllers\ArchiveProject::getProjectLogo(get_the_ID());
@endphp

<article @php post_class('mb-4 border-primary border p-3 rounded') @endphp>
  <div class="row">
    <div class="col-md-3 d-flex align-items-center justify-content-center pt-2 pb-4">
      @if($get_project_logo)
        <a href="{{get_permalink()}}">
          {!! wp_get_attachment_image($get_project_logo, 'medium', false, ['class' => 'w-100 h-auto', 'style' => 'max-width: 350px;']) !!}
        </a>
      @else
        <h2>{!! get_the_title() !!}</h2>
      @endif
    </div>
    <div class="col-md-9">
      <header class="mb-4">
        <time class="updated sr-only" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
        <p class="byline author vcard sr-only">
          {{ __('By', 'sage') }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn">
            {{ get_the_author() }}
          </a>
        </p>

        @include('partials/project-tags')

        <h2 class="entry-title mb-0">
          <a href="{{ get_permalink() }}">{!! get_the_title() !!}</a>
        </h2>

        @if($get_project_duration)
          <small class="d-block mb-2">{{$get_project_duration}}</small>
        @endif

        @if($get_project_collaboration['collaborator_name'])
          Associato a
          <a href="{{$get_project_collaboration['collaborator_url']}}" target="_blank" rel="nofollow">
            <b>{{$get_project_collaboration['collaborator_name']}}</b>
          </a>
        @endif
      </header>
      <div class="entry-summary">
        @php the_excerpt() @endphp
        <ul class="list-unstyled mb-4">
          @if($get_project_kind)
            <li class="mb-3">
              <h5><i class="bi bi-file-earmark-code"></i> Tipo di progetto</h5>
              <p>{{$get_project_kind}}</p>
            </li>
          @endif
          @if($get_project_url)
              <li class="mb-3">
              <h5><i class="bi bi-link-45deg"></i> URL del progetto</h5>
              <a href="{{$get_project_url}}" target="_blank" rel="nofollow">{{$get_project_url}}</a>
            </li>
          @endif
        </ul>
      </div>
      <footer>
        <a href="{{get_permalink()}}" class="btn btn-primary d-block d-md-inline-block">Scopri di più sul progetto <i class="bi bi-arrow-up-right"></i></a>
      </footer>
    </div>
  </div>
</article>
