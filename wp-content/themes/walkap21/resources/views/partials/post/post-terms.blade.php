@if($get_post_terms)
<div class="d-flex align-items-center justify-content-center text-center pb-4">
  <ul class="list-unstyled list-inline">
    @foreach($get_post_terms as $category)
      <li class="list-inline-item">
        <a href="{{get_term_link( $category )}}">{{$category->name}}</a>
      </li>
    @endforeach
  </ul>
</div>
@endif
