@if(have_rows('field_61fd5474b8b72'))
  <section id="sidebar-referrals" class="mb-5">
    <h3 class="mb-3">
      Suggerimenti
    </h3>
    <ul class="list-unstyled">
      @while(have_rows('field_61fd5474b8b72'))
        @php the_row(); @endphp
        @php
          $title = get_sub_field('field_61fd5b2fdae0f');
          $banner_id = get_sub_field('field_61fd548eb8b73');
          $url = get_sub_field('field_61fd5499b8b74');
          $text = get_sub_field('field_61fd5701e1b4f');
        @endphp

        <li class="d-block position-relative">
          <header>
            @if($banner_id)
              <figure class="mb-1">
                {!! wp_get_attachment_image($banner_id, 'sidebar_referrals', false, ['class' => 'w-100 img-fluid']) !!}
              </figure>
            @endif
            @if($title)
              <h4 class="px-3 bg-fluo">
                {{$title}}
              </h4>
            @endif
          </header>
          @if($text)
            <p>
              {{$text}}
            </p>
          @endif
          <a href="{{$url}}" class="stretched-link" target="_blank"></a>
        </li>
      @endwhile
    </ul>
  </section>
@endif
