@if($get_related_posts->have_posts())
  @if($get_related_posts->post_count > 1)
    <section class="related-posts mb-4">
      <h3>Leggi anche questi articoli</h3>
      <div class="swiper-container related-slider" id="related-slider">
        <div class="swiper-wrapper">
          @while( $get_related_posts->have_posts() )
            @php $get_related_posts->the_post() @endphp
            <div class="swiper-slide h-auto">
              <article @php post_class('mb-4 border-primary border p-3 h-100 rounded') @endphp>
                <header>
                  @include('partials/entry-meta')
                  <h4>
                    <a href="{{ get_permalink() }}">
                      {!! get_the_title() !!}
                    </a>
                  </h4>
                </header>
              </article>
            </div>
          @endwhile
        </div>
      </div>
      @php wp_reset_query() @endphp
    </section>
  @endif
@endif
