@if($get_latest_posts->have_posts())
  <section class="mb-4">
    <h2>Articoli più recenti</h2>
    @while($get_latest_posts->have_posts())
      @php $get_latest_posts->the_post(); @endphp
      @include('partials.content-'.get_post_type())
    @endwhile

    @php wp_reset_postdata() @endphp

      <a href="{{get_post_type_archive_link('post')}}" class="btn btn-primary">Vedi tutti gli articoli <i class="bi bi-arrow-up-right"></i></a>
  </section>
@endif



