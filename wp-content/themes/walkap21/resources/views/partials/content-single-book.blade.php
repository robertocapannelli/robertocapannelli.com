<article @php post_class() @endphp itemscope="" itemtype="https://schema.org/Book">
  <div class="row">
    <div class="col-md-3">
      @if(has_post_thumbnail())
        <figure>

          @php $alt_attribute = get_the_title() . ' - ' . (\App\Controllers\SingleBook::getAuthor() ?? ''); @endphp
          <a href="{{\App\Controllers\SingleBook::getAmazonReferral()}}" target="_blank">
            {!! get_the_post_thumbnail(null, 'post-thumbnail', ['class' => 'img-fluid w-100 h-auto', 'itemprop' => 'image', 'alt' => $alt_attribute]) !!}
          </a>
        </figure>
      @endif
    </div>

    <div class="col-md-9">
      <header class="mb-3">
        @include('partials/post-tags')
        <h1 class="entry-title" itemprop="name">
          {!! get_the_title() !!}
        </h1>
        <span class="text-muted d-block" itemprop="author" itemtype="https://schema.org/Person">
          {{\App\Controllers\SingleBook::getAuthor()}}
        </span>
        <span class="text-muted">
          {{\App\Controllers\SingleBook::getPublicationYear()}}
        </span>
      </header>
      <div class="entry-content">
        @php the_content() @endphp
      </div>
      <footer>
        <p>Se ti interessa questo libro acquistalo su Amazon:</p>
        <ul>
          <li>
            @if($amazon_it_url = \App\Controllers\SingleBook::getAmazonReferralIta())
              <b>Versione italiana:</b><br>
              <a href="{{$amazon_it_url}}" class="text-referral" target="_blank">
                {{\App\Controllers\SingleBook::getAmazonReferralTitleIta() ? \App\Controllers\SingleBook::getAmazonReferralTitleIta() : 'Amazon'}}
              </a>
            @else
              Versione italiana non disponibile
            @endif
          </li>

          <li>
            @if($amazon_en_url = \App\Controllers\SingleBook::getAmazonReferral())
              <b>Versione inglese</b><br>
              <a href="{{$amazon_en_url}}" class="text-referral" target="_blank">
                {{\App\Controllers\SingleBook::getAmazonReferralTitle() ? \App\Controllers\SingleBook::getAmazonReferralTitle() : 'Amazon'}}
              </a>
            @else
              Versione inglese non disponibile
            @endif
          </li>
        </ul>
      </footer>
    </div>
  </div>
  @include('partials.book.related-books')
  @php comments_template('/partials/comments.blade.php') @endphp
</article>


