<article @php post_class() @endphp>
  <header class="mb-4">
    <time class="updated sr-only" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
    <p class="byline author vcard sr-only">
      {{ __('By', 'sage') }} <a href="{{ get_author_posts_url(get_the_author_meta('ID')) }}" rel="author" class="fn">
        {{ get_the_author() }}
      </a>
    </p>
    @include('partials/project-tags')
    <h1 class="entry-title mb-0">{!! get_the_title() !!}</h1>
    @if($get_project_duration)
      <small class="d-block mb-2">{{$get_project_duration}}</small>
    @endif
    @if($get_project_collaboration['collaborator_name'])
      Associato a
      <a href="{{$get_project_collaboration['collaborator_url']}}" target="_blank" rel="nofollow">
        <b>{{$get_project_collaboration['collaborator_name']}}</b>
      </a>
    @endif
  </header>
  <div class="entry-content">
    <ul class="list-unstyled mb-4">
      @if($get_project_kind)
        <li class="mb-3">
          <h5><i class="bi bi-file-earmark-code"></i> Tipo di progetto</h5>
          <p>{{$get_project_kind}}</p>
        </li>
      @endif
      @if($get_project_url)
        <li class="mb-3">
          <h5><i class="bi bi-link-45deg"></i> URL del progetto</h5>
          <a href="{{$get_project_url}}" target="_blank" rel="nofollow">{{$get_project_url}}</a>
        </li>
      @endif

      @if($get_project_contributors)
        <li class="mb-3">
          <h5><i class="bi bi-people"></i> Hanno collaborato</h5>
          <ul>
            @foreach($get_project_contributors as $contributor)
              <li>
                <strong>{{$contributor['contributor_name']}}</strong> / {{$contributor['contributor_role']}}
              </li>
            @endforeach
          </ul>
        </li>
      @endif
    </ul>
    @php the_content() @endphp
  </div>
  @include('partials.agency-cta')
</article>


