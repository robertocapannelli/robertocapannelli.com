<!-- Modal -->
<div class="modal fade" id="newsletterModal" data-backdrop="static" tabindex="-1" aria-labelledby="newsletterModalLabel"
     aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered modal-fullscreen-sm-down">
    <div class="modal-content rounded-0">
      <div class="modal-header d-flex align-items-start">
        <h5 class="modal-title" id="newsletterModalLabel">La mia newsletter</h5>

        <button type="button" id="close-newsletter-modal" class="close" data-dismiss="modal" aria-label="Close">
          <i class="bi bi-x-lg"></i>
        </button>
      </div>
      <div class="modal-body">
        <p class="mb-4">
          Ricevi gratuitamente contenuti esclusivi e aggiornamenti sui nuovi articoli. Non saranno troppi! :)
        </p>
        {!! do_shortcode('[contact-form-7 id="7148" title="Newsletter IT"]') !!}
      </div>
    </div>
  </div>
</div>
