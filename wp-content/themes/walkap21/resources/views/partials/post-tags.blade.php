@if(!empty($terms = get_the_terms(null, 'post_tag')))
  @php
    $colors = ['fpurple', 'fpink', 'forange','fgreen','fblue','fgray','fyellow'];
    $array_length = count($colors);
  @endphp
  <div class="posts-tags d-inline-block mb-2">
    in
    @foreach($terms as $key => $term)
      @if($key > $array_length - 1 )
        @php $color = $colors[$key - $array_length];@endphp
      @else
        @php $color = $colors[$key]; @endphp
      @endif
      <a href="{{get_term_link($term->term_id)}}" class="post-tag font-weight-bold{{' bg-' . $color }}">
        {{$term->name}}
      </a>
    @endforeach
  </div>
@endif
