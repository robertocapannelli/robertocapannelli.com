<header class="banner sticky-top Fixed bg-secondary border-primary border-bottom mb-4">
  <div class="container-fluid">
    <nav class="navbar navbar-expand-lg px-0 d-flex justify-content-between">

      <a href="#my-menu" class="mburger mburger--collapse">
        <b></b>
        <b></b>
        <b></b>
      </a>

      <a class="navbar-brand" href="{{ home_url('/') }}">
        <span>R</span>
      </a>
    </nav>
  </div>
</header>
