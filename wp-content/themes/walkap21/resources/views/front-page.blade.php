{{--
Template Name: Front Page
--}}

@extends('layouts.app-front-page')

@section('content')
  @include('partials.post.sticky-posts')
  @include('partials.post.latest-posts')
  @include('partials.book.latest-books')
  @include('partials.post.wordpress-tag-posts')
@endsection
