@extends('layouts.app')

@section('content')
  @php do_action('breadcrumbs') @endphp
  @include('partials.page-header')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Scusami, quello che stai cercando non esiste!', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
@endsection
