@extends('layouts.app')

@section('content')
  @php do_action('breadcrumbs') @endphp

  <div class="page-header">
    <h1>Progetti {!! App::title() !!}</h1>
    <div class="mb-4">
      {!! term_description() !!}
    </div>
  </div>

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while (have_posts()) @php the_post() @endphp
  @include('partials.content-'.get_post_type())
  @endwhile
  @include('partials.agency-cta')
  {!! get_the_posts_navigation() !!}
@endsection
