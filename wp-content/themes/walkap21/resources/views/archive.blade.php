@extends('layouts.app')

@section('content')
  @php do_action('breadcrumbs') @endphp
  @include('partials.page-header')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @include('partials.post.post-terms')

  @while (have_posts()) @php the_post() @endphp
  @include('partials.content-'.get_post_type())
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
