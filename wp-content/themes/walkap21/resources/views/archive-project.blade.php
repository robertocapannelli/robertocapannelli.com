@extends('layouts.app')

@section('content')
  @php do_action('breadcrumbs') @endphp
  @include('partials.page-header')

  <p>In questa pagina potrai trovare alcuni dei progetti che ho sviluppato..<br><br>
    Ho realizzato molti <b>siti web aziendali</b>, <strong>landing page</strong> e soprattutto
    <strong>ecommerce</strong>.<br>
    Utilizzo piattaforme come <strong>Shopify</strong> e <strong>WooCommerce</strong> per sviluppare ecommerce
    performanti e adatti ad ogni esigenza.<br>

    Con <a href="https://supernova.blue">la mia agenzia</a> mi occupo anche di strategie di vendita, <strong>Meta
      ads</strong> e <strong>Google ads</strong>, <strong>Marketing Automation</strong> e <strong>Performance
      Marketing</strong> in generale.<br><br>

    Se hai bisogno di aiuto per un progetto o ti interessano queste tematiche dai un'occhiata.
  </p>

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while (have_posts())
    @php the_post() @endphp
    @include('partials.content-'.get_post_type())
  @endwhile

  @include('partials.agency-cta')
  {!! get_the_posts_navigation() !!}
@endsection
