@extends('layouts.app')

@section('content')
  @php do_action('breadcrumbs') @endphp
  @while(have_posts()) @php the_post() @endphp
  @include('partials.page-header')
    @include('partials.content-page')
  @endwhile
@endsection
