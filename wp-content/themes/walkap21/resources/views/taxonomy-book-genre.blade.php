@extends('layouts.app')

@section('content')
  @php do_action('breadcrumbs') @endphp
  <div class="text-center">
    @include('partials.page-header')
  </div>

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @if($get_archive_description)
    <div class="py-4">
      {!! $get_archive_description !!}
    </div>
  @endif

  @if($get_book_terms)
    <div class="d-flex align-items-center justify-content-center text-center pb-4">
      <ul class="list-unstyled list-inline">
        @foreach($get_book_terms as $book_term)
          <li class="list-inline-item">
            <a href="{{get_term_link( $book_term )}}">{{$book_term->name}}</a>
          </li>
        @endforeach
      </ul>
    </div>
  @endif

  <div class="row">
    @while (have_posts()) @php the_post() @endphp
    @include('partials.content-'.get_post_type())
    @endwhile
  </div>

  {!! get_the_posts_navigation() !!}
@endsection
