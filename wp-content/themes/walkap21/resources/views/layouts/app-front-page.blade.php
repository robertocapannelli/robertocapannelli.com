<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')
<body @php body_class() @endphp>
<div id="my-page">
  @php do_action('get_header') @endphp
  @include('partials.header')
  <div class="wrap container-fluid" role="document">
    <div class="border border-primary p-4 mb-4 rounded bg-white">
      @php the_content() @endphp
    </div>

    <div class="content @if(App\display_sidebar()) row @endif">
      <main class="main @if(App\display_sidebar()) col-lg-8 @endif">
        @yield('content')
      </main>

      @if (App\display_sidebar())
        <aside class="sidebar col-lg-4 mb-4">
          @include('partials.sidebar')
        </aside>
      @endif
    </div>
  </div>
  @php do_action('get_footer') @endphp
  @include('partials.footer')
  @include('partials.mmenu-js')
</div>
@php wp_footer() @endphp
</body>
</html>
