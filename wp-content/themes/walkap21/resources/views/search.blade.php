@extends('layouts.app')

@section('content')
  @php do_action('breadcrumbs') @endphp
  @include('partials.page-header')
  @if (!have_posts())
    <div class="mb-5">
      <div class="alert alert-warning">
        {{ __('Ops! Putroppo non ho ancora scritto un articolo su questo argomento.', 'sage') }}
      </div>
      {!! get_search_form(false) !!}
    </div>
  @endif

  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-search')
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
