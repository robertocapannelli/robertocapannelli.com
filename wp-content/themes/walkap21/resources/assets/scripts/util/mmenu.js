import Mmenu from 'mmenu-js';

function getMmenuJs() {

  let my_menu = document.getElementById('my-menu');

  if (my_menu !== null && typeof my_menu !== 'undefined') {
    new Mmenu('#my-menu', {
        // options
        setSelected: {
          'hover': true,
        },
        extensions: [
          'pagedim-black',
          'fx-listitems-slide',
          'border-full',
        ],
        wrappers: ['bootstrap4'],
        navbar: {
          title: 'Roberto Capannelli',
        },
      },
      {
        // configuration
        offCanvas: {
          page: {
            selector: '#my-page',
          },
        },
        classNames: {
          fixedElements: {
            fixed: 'fix',
          },
        },
      });
  }
}

export {getMmenuJs};
