function openModal() {
  //Check if the cookie is set, if not show the newsletter card
  if (!document.cookie.split(';').filter(function (item) {
    return item.indexOf('is_newsletter_modal_hidden=true') >= 0
  }).length) {
    let bound = 500;
    let isVisible = false;

    //On scroll show the card
    $(window).scroll(function () {
      let currentPosition = $(this).scrollTop();
      let newsletter_modal = $('#newsletterModal');

      if (currentPosition > bound && !isVisible) {
        newsletter_modal.modal('show');
        isVisible = true;
      }
    });
  }
}

function setCookie() {
  let date, expires, path;

  date = new Date();
  date.setTime(date.getTime() + (30 * 24 * 60 * 60 * 1000)); //30 days
  expires = 'expires=' + date.toUTCString() + ';';
  path = 'path=/';
  document.cookie = 'is_newsletter_modal_hidden=true;' + expires + path;
}

function submitForm() {
  let wpcf7_form = document.querySelector('#newsletterModal .wpcf7');
  let newsletter_modal = $('#newsletterModal');

  if (wpcf7_form !== null && typeof wpcf7_form !== 'undefined') {
    wpcf7_form.addEventListener('wpcf7mailsent', function () {
      setCookie();
      setTimeout(function () {
        newsletter_modal.modal('hide');
      }, 2000);
    });
  }
}

function getNewsletterModal() {
  openModal();

  let close_button = document.getElementById('close-newsletter-modal');

  if (close_button !== null && typeof close_button !== 'undefined') {
    close_button.addEventListener('click', function () {
      setCookie();
    });
  }

  submitForm();
}

export {getNewsletterModal}
