import Swiper, {Pagination, Navigation, Autoplay} from 'swiper';

Swiper.use([Pagination, Navigation, Autoplay]);

function getArticlesSlider() {
  let articles_slider = document.getElementById('articles-slider');

  if (articles_slider !== null && typeof articles_slider !== 'undefined') {
    new Swiper('#articles-slider', {
      slidesPerView: 1,
      spaceBetween: 15,
      loop: true,
      parallax: true,
      autoplay: {
        delay: 3500,
        disableOnInteraction: false,
      },
      breakpoints: {
        1200: {
          slidesPerView: 2,
        },
        991: {
          slidesPerView: 2,
        },
        768: {
          slidesPerView: 2,
        },
      },
    });
  }
}

function getBookSlider() {
  let books_slider = document.getElementById('books-slider');

  if (books_slider !== null && typeof books_slider !== 'undefined') {
    new Swiper('#books-slider', {
      slidesPerView: 2,
      spaceBetween: 15,
      loop: true,
      parallax: true,
      autoplay: {
        delay: 3500,
        disableOnInteraction: false,
      },
      breakpoints: {
        1200: {
          slidesPerView: 3,
        },
        991: {
          slidesPerView: 3,
        },
        768: {
          slidesPerView: 3,
        },
      },
    });
  }
}

function getRelatedSlider() {
  let relatedSlider = document.getElementById('related-slider');

  if (relatedSlider !== null && typeof relatedSlider !== 'undefined') {
    new Swiper('.related-slider', {
      slidesPerView: 2,
      spaceBetween: 15,
      loop: true,
      parallax: true,
      autoplay: {
        delay: 3500,
        disableOnInteraction: false,
      },
      breakpoints: {
        1200: {
          slidesPerView: 4,
        },
        991: {
          slidesPerView: 4,
        },
        768: {
          slidesPerView: 4,
        },
      },
    });
  }
}

export {getArticlesSlider, getBookSlider, getRelatedSlider};
